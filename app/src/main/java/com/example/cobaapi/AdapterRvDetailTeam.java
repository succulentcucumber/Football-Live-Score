package com.example.cobaapi;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cobaapi.R;
import com.example.cobaapi.SkorItem;
import com.example.cobaapi.TeamItem;

import java.util.ArrayList;

public class AdapterRvDetailTeam extends RecyclerView.Adapter<AdapterRvDetailTeam.RvDetailTeamViewHolder> {

    ArrayList<SkorItem> listSkor = new ArrayList<SkorItem>();
    TeamItem tim;
    private OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(AdapterRvDetailTeam.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }



    public AdapterRvDetailTeam(ArrayList<SkorItem> listSkor, TeamItem tim) {
        this.listSkor = listSkor;
        this.tim = tim;
    }

    @NonNull
    @Override
    public AdapterRvDetailTeam.RvDetailTeamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.latest_score, parent, false);
        return new AdapterRvDetailTeam.RvDetailTeamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterRvDetailTeam.RvDetailTeamViewHolder holder, int position) {
        holder.namaHome.setText(listSkor.get(position).getHomeName());
        holder.namaAway.setText(listSkor.get(position).getAwayName());
        holder.skorHome.setText(listSkor.get(position).getHomeSkor()+"");
        holder.skorAway.setText(listSkor.get(position).getAwaySkor()+"");
        holder.league.setText(listSkor.get(position).getLeague());
        holder.tanggal.setText(listSkor.get(position).getTanggal());
        holder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                onItemClickCallback.onClick(
                        listSkor.get(holder.getAdapterPosition())
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class RvDetailTeamViewHolder extends RecyclerView.ViewHolder {
        TextView namaHome,namaAway,skorHome,skorAway,tanggal,league;
        ConstraintLayout item;
        public RvDetailTeamViewHolder(@NonNull View itemView) {
            super(itemView);
            namaHome=itemView.findViewById(R.id.tvHomeTeam);
            namaAway=itemView.findViewById(R.id.tvNamaAway);
            skorHome=itemView.findViewById(R.id.tvScoreHome);
            skorAway=itemView.findViewById(R.id.tvScoreAway);
            item=itemView.findViewById(R.id.itemSkor);
            tanggal=itemView.findViewById(R.id.tvMatchTanggal);
            league = itemView.findViewById(R.id.tvMatchLeague);
        }
    }

    public interface OnItemClickCallback{
        void onClick(SkorItem item);
    }
}
