package com.example.cobaapi;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface userDAO {

    @Insert
    public void insert(user... user);
    @Update
    public void update(user... user);
    @Delete
    public void delete(user user);

    @Query("SELECT * FROM tUser")
    List<user> getUsers();


}
