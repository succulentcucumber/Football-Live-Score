package com.example.cobaapi;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeamFragment extends Fragment {
    RecyclerView rv;
    EditText edSearch;
    public TeamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_team, container, false);

        edSearch = (EditText) v.findViewById(R.id.edSearchBar);
        rv = (RecyclerView) v.findViewById(R.id.rvBrowseTeam);
        ArrayList<TeamItem> listTeam = ((MainActivity) getActivity()).listTeam;
        AdapterRvTeams adapter = new AdapterRvTeams(listTeam);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(adapter);

        adapter.setOnItemClickCallback(new AdapterRvTeams.OnItemClickCallback() {
            @Override
            public void onClick(TeamItem item) {
                ((MainActivity) getActivity()).currTeam=item;
                ((MainActivity) getActivity()).openFragment(new DetailTeamFragment());
            }
        });

        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int j, int i1, int i2) {
                String text = edSearch.getText().toString();
                ArrayList<TeamItem> utama = ((MainActivity) getActivity()).listTeam;
                ArrayList<TeamItem> temp = new ArrayList<TeamItem>();
                for (int i=0;i<utama.size();i++){
                    if (utama.get(i).getNama().toLowerCase().contains(text.toLowerCase())){
                        temp.add(utama.get(i));

                        AdapterRvTeams adapter = new AdapterRvTeams(temp);
                        rv.setAdapter(adapter);

                        adapter.setOnItemClickCallback(new AdapterRvTeams.OnItemClickCallback() {
                            @Override
                            public void onClick(TeamItem item) {
                                ((MainActivity) getActivity()).currTeam=item;
                                ((MainActivity) getActivity()).openFragment(new DetailTeamFragment());
                            }
                        });
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return v;
    }

}
