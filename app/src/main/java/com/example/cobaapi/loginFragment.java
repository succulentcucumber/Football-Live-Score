package com.example.cobaapi;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class loginFragment extends Fragment {

    EditText edUsername, edPassword;
    Button btnLogin,btnRegister;
    public loginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment, container, false);

        edUsername = (EditText) v.findViewById(R.id.edUsernameLogin);
        edPassword = (EditText) v.findViewById(R.id.edPasswordLogin);
        btnLogin = (Button) v.findViewById(R.id.btnLoginLogin);
        btnRegister = (Button)v.findViewById(R.id.btnLoginRegister);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edUsername.getText().toString();
                String password = edPassword.getText().toString();

                ArrayList<user> listUser = ((loginRegis) getActivity()).listUser;
                    boolean ada = false;
                    for (int i=0;i<listUser.size();i++){
                        if (username.equals(listUser.get(i).username)){
                            if (password.equals(listUser.get(i).password)){
                                ada = true;

                                ((loginRegis) getActivity()).openHome(listUser.get(i));

                                break;
                            }
                        }
                    }
                    if (ada == false){
                        Toast.makeText(getContext(), "User yang Anda Cari Tidak Ada", Toast.LENGTH_SHORT).show();
                    }
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((loginRegis) getActivity()).openFragment(new registerFragment());
            }
        });
        return v;
    }

}
