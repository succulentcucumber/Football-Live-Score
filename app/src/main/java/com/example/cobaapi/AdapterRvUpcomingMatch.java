package com.example.cobaapi;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterRvUpcomingMatch extends RecyclerView.Adapter<AdapterRvUpcomingMatch.RvUpcomingMatchViewHolder> {

    ArrayList<SkorItem> listSkor = new ArrayList<SkorItem>();
    TeamItem tim;

    public AdapterRvUpcomingMatch(ArrayList<SkorItem> listSkor, TeamItem tim) {
        this.listSkor = listSkor;
        this.tim = tim;
    }

    @NonNull
    @Override
    public AdapterRvUpcomingMatch.RvUpcomingMatchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.upcoming_match, parent, false);
        return new AdapterRvUpcomingMatch.RvUpcomingMatchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRvUpcomingMatch.RvUpcomingMatchViewHolder holder, int position) {
        holder.namaHome.setText(listSkor.get(position).getHomeName());
        holder.namaAway.setText(listSkor.get(position).getAwayName());
        holder.league.setText(listSkor.get(position).getLeague());
        holder.tanggal.setText(listSkor.get(position).getTanggal());
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class RvUpcomingMatchViewHolder extends RecyclerView.ViewHolder {
        TextView namaHome,namaAway,tanggal,league;
        ConstraintLayout item;
        public RvUpcomingMatchViewHolder(@NonNull View itemView) {
            super(itemView);
            namaHome=itemView.findViewById(R.id.tvUpHomeTeam);
            namaAway=itemView.findViewById(R.id.tvUpNamaAway);
            item=itemView.findViewById(R.id.itemUpcoming);
            tanggal=itemView.findViewById(R.id.tvUpMatchTanggal);
            league = itemView.findViewById(R.id.tvUpMatchLeague);
        }
    }
}
