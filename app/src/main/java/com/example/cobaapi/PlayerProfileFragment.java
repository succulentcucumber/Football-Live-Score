package com.example.cobaapi;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlayerProfileFragment extends Fragment {


    public PlayerProfileFragment() {
        // Required empty public constructor
    }


    ImageView implayer;
    TextView namep,nationp,posp,birthp,weigthp,heightp,teamp,descp;

    matchplayer player;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.player_profile, container, false);
        implayer= v.findViewById(R.id.imPlayer);
        namep= v.findViewById(R.id.tvPlayerName);
        nationp= v.findViewById(R.id.tvPlayerNation);
        posp= v.findViewById(R.id.tvPlayerPos);
        birthp= v.findViewById(R.id.tvPlayerBirth);
        weigthp= v.findViewById(R.id.tvPlayerW);
        heightp= v.findViewById(R.id.tvPlayerH);
        teamp= v.findViewById(R.id.tvPlayerTeam);
        descp= v.findViewById(R.id.tvPlayerDesc);
        player= ((MainActivity)getActivity()).player;

        namep.setText(player.getNamap());
        nationp.setText("Nationality: "+player.getNationp());
        posp.setText( "Position: "+player.getStatp());
        birthp.setText("Birthday: "+player.getBplacep()+"/"+player.getBornp());
        weigthp.setText("Weight: "+player.getWeightp());
        heightp.setText("Height: "+player.getHeightp());
        teamp.setText("Team: "+player.getTeamp());
        descp.setText(player.getDescp());
        loadPlayerImg(player.getNamap());
        return v;
    }

    public void loadPlayerImg(String nama){
        nama= nama.replaceAll("\\s+","_");
        String url =  "https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?p="+nama;
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("player");
                    object=jsonArray.getJSONObject(0);
                    String url = object.getString("strThumb");
                    new PlayerProfileFragment.DownloadImage(implayer)
                            .execute(url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(stringRequest);
    }


    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


}
