package com.example.cobaapi;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class LiveFragment extends Fragment {


    public LiveFragment() {
        // Required empty public constructor
    }

    RecyclerView rv;
    AdapterRvLiveScore adapter;
    ArrayList<SkorItem> listSkor = new ArrayList<SkorItem>();
    Timer timer = new Timer();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_live, container, false);
        rv = v.findViewById(R.id.rv_livescore);
        String url = "https://www.thesportsdb.com/api/v1/json/4013017/latestsoccer.php";
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    final JSONArray jsonArray = object.getJSONObject("teams").getJSONArray("Match");
                    for(int i=0;i<jsonArray.length();i++){
                        listSkor.add(new SkorItem(jsonArray.getJSONObject(i),2));
                    }
                    adapter = new AdapterRvLiveScore(listSkor);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                    rv.setLayoutManager(layoutManager);
                    rv.setAdapter(adapter);

                    adapter.setOnItemClickCallback(new AdapterRvLiveScore.OnItemClickCallback() {
                        @Override
                        public void onClick(SkorItem item,int position) {
                            ((MainActivity)getActivity()).pos=position;
                            ((MainActivity)getActivity()).live=1;
                            ((MainActivity) getActivity()).currSkor=item;
                            ((MainActivity) getActivity()).openFragment(new DetailSkorFragment());
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getActivity(), "No Live Score Data Available", Toast.LENGTH_SHORT).show();
                    String url = "https://www.thesportsdb.com/api/v1/json/4013017/latestsoccer.php";
                    System.out.println(url);

                    try{
                        JSONObject object = new JSONObject(response);
                        JSONObject jsonObject = object.getJSONObject("teams").getJSONObject("Match");
                        listSkor.add(new SkorItem(jsonObject,2));

                        adapter = new AdapterRvLiveScore(listSkor);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(adapter);

                        adapter.setOnItemClickCallback(new AdapterRvLiveScore.OnItemClickCallback() {
                            @Override
                            public void onClick(SkorItem item,int position) {
                                ((MainActivity)getActivity()).live=1;
                                ((MainActivity) getActivity()).currSkor=item;
                                ((MainActivity) getActivity()).openFragment(new DetailSkorFragment());
                            }
                        });
                    }catch (Exception e1){
                        e1.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error load team", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(stringRequest);


        return v;
    }
}
