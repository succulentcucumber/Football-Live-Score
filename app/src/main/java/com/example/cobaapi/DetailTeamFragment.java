package com.example.cobaapi;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailTeamFragment extends Fragment {

    TeamItem team;
    TextView nama,negara,venue,web;
    ImageView logo;
    private String url;
    RecyclerView rv;
    RecyclerView rv1;
    AdapterRvDetailTeam adapter;
    AdapterRvUpcomingMatch adapter1;
    ArrayList<SkorItem> listSkor = new ArrayList<SkorItem>();
    ArrayList<SkorItem> listUpcoming = new ArrayList<SkorItem>();
    Button btnFav;
    Menu menu;


    public DetailTeamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_team, container, false);

        team = ((MainActivity) getActivity()).currTeam;

        nama = v.findViewById(R.id.tvTeamNama);
        negara=v.findViewById(R.id.tvTeamNegara);
        venue=v.findViewById(R.id.tvTeamVanue);
        web=v.findViewById(R.id.tvTeamWebsite);
        logo=v.findViewById(R.id.ivTeamLogo);
        rv=v.findViewById(R.id.rvDetailTeamSkor);
        rv1=(RecyclerView)v.findViewById(R.id.rvUpcomingMatch);
        setHasOptionsMenu(true);
        btnFav=v.findViewById(R.id.menu_favorite);

        nama.setText(team.getNama());
        negara.setText(team.getNegara());
        venue.setText(team.getVanue());
        web.setText(team.getWebsite());
            url = team.getLogo();

        new DownloadImageTask((ImageView) logo).execute(url);
        rv = (RecyclerView) v.findViewById(R.id.rvDetailTeamSkor);

        adapter = new AdapterRvDetailTeam(listSkor,team);

        loadLatestScore(team.getId());
        loadUpcomingMatch(team.getId());

        return v;

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public void loadLatestScore(int teamid){
        listSkor.clear();
        String url = " https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id="+teamid;
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("results");
                    for(int i=0;i<jsonArray.length();i++){
                        listSkor.add(new SkorItem(jsonArray.getJSONObject(i),0));
                    }
                    AdapterRvDetailTeam adapter = new AdapterRvDetailTeam(listSkor,team);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                    rv.setLayoutManager(layoutManager);
                    rv.setAdapter(adapter);

                    adapter.setOnItemClickCallback(new AdapterRvDetailTeam.OnItemClickCallback() {
                        @Override
                        public void onClick(SkorItem item) {
                            ((MainActivity)getActivity()).live=0;
                            ((MainActivity) getActivity()).currSkor=item;
                            ((MainActivity) getActivity()).openFragment(new DetailSkorFragment());
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error load latest skor", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(stringRequest);
    }

    public void loadUpcomingMatch(int teamid){
        listUpcoming.clear();
        String url = "https://www.thesportsdb.com/api/v1/json/1/eventsnext.php?id="+teamid;
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("events");
                    for(int i=0;i<jsonArray.length();i++){
                        listUpcoming.add(new SkorItem(jsonArray.getJSONObject(i),1));
                    }
                    adapter1 = new AdapterRvUpcomingMatch(listUpcoming,team);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                    rv1.setLayoutManager(layoutManager);
                    rv1.setAdapter(adapter1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error load upcoming match", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(stringRequest);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_detailteam,menu);
        this.menu=menu;
        //menu.findItem(R.id.menu_fav).setIcon()
        if(((MainActivity) getActivity()).listFav.contains(((MainActivity) getActivity()).currTeam)){
            menu.findItem(R.id.menu_fav).setIcon(R.drawable.ic_favorite_black_24dp);
        }else{
            menu.findItem(R.id.menu_fav).setIcon(R.drawable.ic_favorite_border_black_24dp);
        }


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        return super.onOptionsItemSelected(item);
    }
}
