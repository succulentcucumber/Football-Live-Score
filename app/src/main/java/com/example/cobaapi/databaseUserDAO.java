package com.example.cobaapi;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {user.class}, version = 1, exportSchema = false)
public abstract class databaseUserDAO extends RoomDatabase {
    public abstract userDAO getUserDAO();
}
