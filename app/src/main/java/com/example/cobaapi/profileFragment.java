package com.example.cobaapi;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class profileFragment extends Fragment {

    EditText edUsername, edNama, edPassword;
    Button tombolEdit;
    public profileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        edUsername = view.findViewById(R.id.edEditUsername);
        edNama = view.findViewById(R.id.edEditName);
        edPassword = view.findViewById(R.id.edEditPassword);
        tombolEdit = view.findViewById(R.id.btnEditProfile);

        edUsername.setText(MainActivity.currUser.getUsername());
        edNama.setText(MainActivity.currUser.getNama());
        edPassword.setText(MainActivity.currUser.getPassword());

        tombolEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.currUser.setUsername(edUsername.getText().toString());
                MainActivity.currUser.setNama(edNama.getText().toString());
                MainActivity.currUser.setPassword(edPassword.getText().toString());
                ((MainActivity) getActivity()).updateUser(MainActivity.currUser);
                //Toast.makeText(getActivity(), "Edit Profile Telah Tersimpan!", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }
}
