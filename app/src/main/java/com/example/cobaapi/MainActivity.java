package com.example.cobaapi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.app.ActionBar;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.drm.ProcessedData;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenu;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    ArrayList<league> listLeague = new ArrayList<league>();
    ArrayList<TeamItem> listTeam = new ArrayList<TeamItem>();
    BottomNavigationView navbar;
    ArrayList<TeamItem> listFav = new ArrayList<TeamItem>();
    matchplayer player;
    TeamItem currTeam;
    databaseFavDao databaseFavDao;
    databaseUserDAO databaseUserDAO;
    favoriteDAO favoriteDAO;
    userDAO userDAO;
    Fragment fr;
    public static user currUser;
    SkorItem currSkor;
    int live;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        centerTitle();

        currUser = getIntent().getParcelableExtra("currUser");

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Waiting leagues");



        navbar=findViewById(R.id.bottomNavUtama);
        navbar.setOnNavigationItemSelectedListener(bottomNavListener);

        databaseFavDao = Room.databaseBuilder(this,databaseFavDao.class,"FavDB").allowMainThreadQueries().build();
        favoriteDAO=databaseFavDao.getFavoriteDAO();
        databaseUserDAO = Room.databaseBuilder(this, databaseUserDAO.class, "userDB").allowMainThreadQueries().build();
        userDAO = databaseUserDAO.getUserDAO();
        refreshFav();




        if(getIntent().getStringExtra("notif")!=null){
            getIntent().removeExtra("notif");
            openFragment(new LiveFragment());
        }else{
            addLeagueList();
        }
    }

    private void centerTitle() {
        ArrayList<View> textViews = new ArrayList<>();

        getWindow().getDecorView().findViewsWithText(textViews, getTitle(), View.FIND_VIEWS_WITH_TEXT);

        if(textViews.size() > 0) {
            AppCompatTextView appCompatTextView = null;
            if(textViews.size() == 1) {
                appCompatTextView = (AppCompatTextView) textViews.get(0);
            } else {
                for(View v : textViews) {
                    if(v.getParent() instanceof Toolbar) {
                        appCompatTextView = (AppCompatTextView) v;
                        break;
                    }
                }
            }

            if(appCompatTextView != null) {
                ViewGroup.LayoutParams params = appCompatTextView.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                appCompatTextView.setLayoutParams(params);
                appCompatTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
        }
    }

    public void addLeagueList(){
        listLeague.clear();
        loadLeagueByID("4328");
        loadLeagueByID("4335");
        loadLeagueByID("4332");
        loadLeagueByID("4331");
        loadLeagueByID("4336");

    }


    private BottomNavigationView.OnNavigationItemSelectedListener bottomNavListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            if(menuItem.getItemId()==R.id.menu_browse){
                openFragment(new browseFragment());
            }
            if(menuItem.getItemId()==R.id.menu_favorite){
                openFragment(new favoriteFragment());
            }
            if(menuItem.getItemId()==R.id.navbar_live){
                openFragment(new LiveFragment());
            }
            if (menuItem.getItemId()==R.id.menu_profile)
            {
                openFragment(new profileFragment());
            }
            return true;
        }
    };

    public void openFragment(Fragment fr){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentUtama, fr,fr.getId()+"");
        ft.addToBackStack(fr.getId()+"");
        ft.commit();
    }

    public void loadPlayerProfile(final String nama){
        Toast.makeText(this, "INI BISA", Toast.LENGTH_SHORT).show();
        String namaaf= nama.replaceAll("\\s+","_");
        String url="https://www.thesportsdb.com/api/v1/json/4013017/searchplayers.php?p="+namaaf;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("player");
                    object=jsonArray.getJSONObject(0);
                    String image = object.getString("strCutout");
                    String nama = object.getString("strPlayer");
                    String stat = object.getString("strPosition");
                    String team = object.getString("strTeam");
                    String desc = object.getString("strDescriptionEN");
                    String weight= object.getString("strWeight");
                    String height = object.getString("strHeight");
                    String born = object.getString("dateBorn");
                    String loc = object.getString("strBirthLocation");
                    String nation = object.getString("strNationality");
                    player= new matchplayer(image,nama,stat,team,desc,weight,height,born,loc,nation);
                    fr= new PlayerProfileFragment();
                    openFragment(fr);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(stringRequest);
    }

    public void loadLeagueByID(final String idLeague){
        String url = "https://www.thesportsdb.com/api/v1/json/1/lookupleague.php?id=" + idLeague;
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("leagues");
                    listLeague.add(new league(jsonArray.getJSONObject(0)));
                    //Toast.makeText(getApplicationContext(), "Isi League : " + listLeague.size(), Toast.LENGTH_SHORT).show();
                    openFragment(new browseFragment());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Error load league", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(stringRequest);

    }

    public void loadTeam(int id){
        listTeam.clear();
        String url = "https://www.thesportsdb.com/api/v1/json/1/lookup_all_teams.php?id=" + id;
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("teams");
                    for(int i=0;i<jsonArray.length();i++){
                        listTeam.add(new TeamItem(jsonArray.getJSONObject(i)));
                    }

                    fr = new TeamFragment();
                    openFragment(fr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Error load team", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(this);
        rq.add(stringRequest);
    }

    public void insertFav(TeamItem baru){
        baru.setUsername(currUser.getUsername());
        favoriteDAO.insert(baru);
        refreshFav();
        Toast.makeText(this, "Team added to favorite list!", Toast.LENGTH_SHORT).show();
    }
    public void refreshFav(){
        listFav.clear();
        listFav.addAll(favoriteDAO.getFavorites(currUser.getUsername()));

        refreshService();
    }

    public void refreshService(){
        Intent intent = new Intent(this,BackgroundService.class);
        intent.putParcelableArrayListExtra("listFav",listFav);
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Context context;
        Resources resources;
        switch (item.getItemId())
        {
            case R.id.item_language:
                Intent intent = new Intent();
                intent.setComponent( new ComponentName("com.android.settings","com.android.settings.Settings$InputMethodAndLanguageSettingsActivity" ));
                startActivity(intent);
//                if (LocaleHelper.getLanguage(MainActivity.this).equalsIgnoreCase("en"))
//                {
//                    context = LocaleHelper.setLocale(MainActivity.this, "in");
//                    resources = context.getResources();
//                }
//                else if (LocaleHelper.getLanguage(MainActivity.this).equalsIgnoreCase("in"))
//                {
//                    context = LocaleHelper.setLocale(MainActivity.this, "en");
//                    resources = context.getResources();
//                }
//                finish();
//                startActivity(getIntent());
                break;
            case R.id.item_logout:
                Intent toLogin = new Intent(MainActivity.this, loginRegis.class);
                for(int i=0;i<getFragmentManager().getBackStackEntryCount();i++){
                    getFragmentManager().popBackStack();
                }
                finish();
                startActivity(toLogin);
                break;
            case R.id.menu_fav:
                Boolean nemu = false;
                for(int i=0;i<listFav.size();i++){
                    if(listFav.get(i).getId()==currTeam.getId()) {
                        listFav.remove(i);
                        favoriteDAO.delete(currTeam);
                        nemu=true;
                        refreshService();

                    }
                }
                if(!nemu){
                    currTeam.setUsername(currUser.getUsername());
                    favoriteDAO.insert(currTeam);
                    listFav.add(currTeam);
                    invalidateOptionsMenu();
                    refreshService();
                    //btnFav.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
                    //MenuItem btnFav = menu(R.id.menu_fav);
                }
                invalidateOptionsMenu();
                break;
        }
        return true;
    }

    public void updateUser(user updateUser){
        userDAO.update(updateUser);
        Toast.makeText(this, "Update Success!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount()>0){
            getFragmentManager().popBackStack();
        }else{
            super.onBackPressed();
        }
    }
}
