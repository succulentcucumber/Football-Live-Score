package com.example.cobaapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;
import java.util.ArrayList;

public class AdapterRVAplayer extends RecyclerView.Adapter<AdapterRVAplayer.RVAplayerViewHolder> {

    ArrayList<matchplayer> aplayer= new ArrayList<>();

    public AdapterRVAplayer(ArrayList<matchplayer> aplayer) {
        this.aplayer = aplayer;
    }

    private AdapterRVAplayer.OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(AdapterRVAplayer.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }


    @NonNull
    @Override
    public RVAplayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View view= inflater.inflate(R.layout.playerinmatch,parent,false);

        return new AdapterRVAplayer.RVAplayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RVAplayerViewHolder holder, int position) {
        new AdapterRVAplayer.DownloadImageTask((ImageView) holder.imPlayer)
                .execute(aplayer.get(position).getImgp());
        holder.nPlayer.setText(aplayer.get(position).getNamap());
        holder.sPlayer.setText(aplayer.get(position).getStatp());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onClick(
                        aplayer.get(holder.getAdapterPosition())
                );
            }
        });
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public int getItemCount() {
        return aplayer.size();
    }

    public class RVAplayerViewHolder extends RecyclerView.ViewHolder {

        ImageView imPlayer;
        TextView nPlayer,sPlayer;
        public RVAplayerViewHolder(@NonNull View itemView) {
            super(itemView);
            imPlayer= itemView.findViewById(R.id.imPlayer);
            nPlayer= itemView.findViewById(R.id.tvHplayer);
            sPlayer= itemView.findViewById(R.id.statHplayer);
        }
    }

    public interface OnItemClickCallback{
        void onClick(matchplayer mp);
    }
}
