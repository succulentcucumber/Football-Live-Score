package com.example.cobaapi;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {TeamItem.class}, version = 1, exportSchema = false)
public abstract class databaseFavDao extends RoomDatabase {
    public abstract favoriteDAO getFavoriteDAO();
}
