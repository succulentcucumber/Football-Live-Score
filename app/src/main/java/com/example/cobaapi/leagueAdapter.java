package com.example.cobaapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;
import java.util.ArrayList;

public class leagueAdapter extends RecyclerView.Adapter<leagueAdapter.leagueAdapterViewHolder> {

    ArrayList<league> listLeague;
    private leagueAdapter.OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(leagueAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }
    public leagueAdapter(ArrayList<league> listLeague){
        this.listLeague = listLeague;
    }
    @NonNull
    @Override
    public leagueAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_league, parent, false);
        return new leagueAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final leagueAdapterViewHolder holder, int position) {
        holder.tvNama.setText(listLeague.get(position).strLeague);
        holder.tvCountry.setText(listLeague.get(position).counrty);

        new DownloadImageTask((ImageView) holder.logo)
                .execute(listLeague.get(position).logo);

        holder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                onItemClickCallback.onClick(
                        listLeague.get(holder.getAdapterPosition())
                );
            }
        });
        System.out.println(listLeague.get(position).logo);
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public int getItemCount() {
        return listLeague.size();
    }

    public class leagueAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView tvNama, tvCountry;
        ImageView logo;
        public leagueAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logoLeague);
            tvNama = itemView.findViewById(R.id.tv_titleLeague);
            tvCountry = itemView.findViewById(R.id.tv_country);
        }
    }
    public interface OnItemClickCallback{
        void onClick(league item);
    }
}
