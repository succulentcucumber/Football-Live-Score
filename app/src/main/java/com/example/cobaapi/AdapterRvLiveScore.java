package com.example.cobaapi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterRvLiveScore extends RecyclerView.Adapter<AdapterRvLiveScore.RvLiveScoreViewHolder> {

    ArrayList<SkorItem> listSkor = new ArrayList<SkorItem>();
    private OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(AdapterRvLiveScore.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public AdapterRvLiveScore(ArrayList<SkorItem> listSkor) {
        this.listSkor = listSkor;
    }

    @NonNull
    @Override
    public AdapterRvLiveScore.RvLiveScoreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.live_score, parent, false);
        return new AdapterRvLiveScore.RvLiveScoreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterRvLiveScore.RvLiveScoreViewHolder holder, int position) {
        holder.namaHome.setText(listSkor.get(position).getHomeName());
        holder.namaAway.setText(listSkor.get(position).getAwayName());
        holder.skorHome.setText(listSkor.get(position).getHomeSkor()+"");
        holder.skorAway.setText(listSkor.get(position).getAwaySkor()+"");
        holder.league.setText(listSkor.get(position).getLeague());
        holder.tanggal.setText(listSkor.get(position).getTanggal());
        holder.time.setText(listSkor.get(position).getTime());
        holder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                onItemClickCallback.onClick(
                        listSkor.get(holder.getAdapterPosition()),
                        holder.getAdapterPosition()
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        return listSkor.size();
    }

    public class RvLiveScoreViewHolder extends RecyclerView.ViewHolder {
        TextView namaHome,namaAway,skorHome,skorAway,tanggal,league,time;
        ConstraintLayout item;
        public RvLiveScoreViewHolder(@NonNull View itemView) {
            super(itemView);
            namaHome=itemView.findViewById(R.id.tvLiveHomeTeam);
            namaAway=itemView.findViewById(R.id.tvLiveNamaAway);
            skorHome=itemView.findViewById(R.id.tvLiveScoreHome);
            skorAway=itemView.findViewById(R.id.tvLiveScoreAway);
            item=itemView.findViewById(R.id.itemLiveSkor);
            tanggal=itemView.findViewById(R.id.tvLiveMatchTanggal);
            league = itemView.findViewById(R.id.tvLiveMatchLeague);
            time=itemView.findViewById(R.id.tvLiveTime);
        }
    }

    public interface OnItemClickCallback{
        void onClick(SkorItem item,int pos);
    }
}
