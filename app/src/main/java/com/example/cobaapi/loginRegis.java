package com.example.cobaapi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class loginRegis extends AppCompatActivity {

    ArrayList<user> listUser = new ArrayList<user>();
    databaseUserDAO databaseUserDAO;
    userDAO userDAO;
    user userLogin;

    BottomNavigationView bottomNav;
    Fragment fr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_regis);

        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        databaseUserDAO = Room.databaseBuilder(this, databaseUserDAO.class, "userDB").allowMainThreadQueries().build();
        userDAO = databaseUserDAO.getUserDAO();

        refreshUser();

        fr = new loginFragment();
        openFragment(fr);
    }
    public void openHome(user userLogin){
        this.userLogin = userLogin;
        Intent inten = new Intent(loginRegis.this, MainActivity.class);
        inten.putExtra("currUser",userLogin);
        finish();
        startActivity(inten);

    }
    public void insertUser(user baru){
        userDAO.insert(baru);
        refreshUser();
        Toast.makeText(this, "behasil menambahkan User baru", Toast.LENGTH_SHORT).show();
    }
    public void refreshUser(){
        listUser.clear();
        listUser.addAll(userDAO.getUsers());
    }
    public void openFragment(Fragment fr){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.containerLoginRegis, fr);
        ft.commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener bottomNavListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.menu_login:
                    fr = new loginFragment();
                    openFragment(fr);
                    break;
                case R.id.menu_register:
                    fr = new registerFragment();
                    openFragment(fr);
                    break;
            }
            return false;
        }
    };
}
