package com.example.cobaapi;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailSkorFragment extends Fragment {

    TextView skorHome,skorAway,namaHome,namaAway,tableSkorHome,tableSkorAway,shotsHome,shotsAway,yellowHome,yellowAway,redHome,redAway,listGoalHome,listGoalAway;
    ImageView logoHome,logoAway;
    RecyclerView rvHP,rvAP;
    AdapterRVHplayer rvha;
    AdapterRVAplayer rvaa;
    ArrayList<matchplayer> hp= new ArrayList<>();
    ArrayList<matchplayer> ap= new ArrayList<>();
    TextView playerH,playerA;
    int live;
    public DetailSkorFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_skor, container, false);
        live=((MainActivity)getActivity()).live;
        playerH= v.findViewById(R.id.tvHome);
        playerA= v.findViewById(R.id.tvAway);
        skorHome= v.findViewById(R.id.tvDSSkorHome);
        skorAway=v.findViewById(R.id.tvDSSkorAway);
        namaHome = v.findViewById(R.id.tvDSNamaHome);
        namaAway=v.findViewById(R.id.tvDSNamaAway);
        tableSkorHome=v.findViewById(R.id.tvDSTSkorHome);
        tableSkorAway=v.findViewById(R.id.tvDSTSkorAway);
        shotsHome=v.findViewById(R.id.tvDSTShotsHome);
        shotsAway=v.findViewById(R.id.tvDSTAwayShots);
        yellowAway=v.findViewById(R.id.tvDSTYellowAway);
        yellowHome=v.findViewById(R.id.tvDSTYellowHome);
        redHome=v.findViewById(R.id.tvDSTRedHome);
        redAway=v.findViewById(R.id.tvDSTRedAway);
        logoHome=v.findViewById(R.id.ivDSLogoHome);
        logoAway=v.findViewById(R.id.ivDSLogoAway);
        listGoalHome=v.findViewById(R.id.tvDSListHome);
        listGoalAway=v.findViewById(R.id.tvDSListAway);

        skorHome.setText(((MainActivity) getActivity()).currSkor.getHomeSkor()+"");
        skorAway.setText(((MainActivity) getActivity()).currSkor.getAwaySkor()+"");
        namaHome.setText(((MainActivity) getActivity()).currSkor.getHomeName());
        namaAway.setText(((MainActivity) getActivity()).currSkor.getAwayName());
        tableSkorHome.setText(((MainActivity) getActivity()).currSkor.getHomeSkor()+"");
        tableSkorAway.setText(((MainActivity) getActivity()).currSkor.getAwaySkor()+"");
        playerH.setText(namaHome.getText().toString() + " Squad: ");
        playerA.setText(namaAway.getText().toString() + " Squad: ");

        rvHP= (RecyclerView)v.findViewById(R.id.rvHP);
        rvHP.setHasFixedSize(true);
        rvAP= (RecyclerView)v.findViewById(R.id.rvAP);
        rvAP.setHasFixedSize(true);
        loadMatchData();
        loadMatchPlayer();
        return v;
    }

    public void loadMatchPlayer(){
            String nHome= namaHome.getText().toString().replaceAll("\\s+","_");
            String url= "https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?t="+nHome;
            System.out.println(url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        JSONArray jsonArray = object.getJSONArray("player");
                        for (int i=0; i< jsonArray.length(); i++){
                            object=jsonArray.getJSONObject(i);
                            String image = object.getString("strCutout");
                            String nama = object.getString("strPlayer");
                            String stat = object.getString("strPosition");
                            String team = object.getString("strTeam");
                            String desc = object.getString("strDescriptionEN");
                            String weight= object.getString("strTeam");
                            String height = object.getString("strTeam");
                            String born = object.getString("dateBorn");
                            String loc = object.getString("strBirthLocation");
                            String nation = object.getString("strNationality");
                            hp.add(new matchplayer(image,nama,stat,team,desc,weight,height,born,loc,nation));
                            System.out.println(hp.get(i).getNamap());
                        }
                        rvha= new AdapterRVHplayer(hp);
                        rvha.notifyDataSetChanged();
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                        rvHP.setLayoutManager(layoutManager);
                        rvHP.setAdapter(rvha);
                        rvha.setOnItemClickCallback(new AdapterRVHplayer.OnItemClickCallback() {
                            @Override
                            public void onClick(matchplayer mp) {
                                try {
                                    ((MainActivity)getActivity()).loadPlayerProfile(mp.getNamap());
                                    Toast.makeText(getContext(), mp.getNamap(), Toast.LENGTH_SHORT).show();
                                }catch (Exception e){
                                    e.getMessage();
                                }
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            });
            RequestQueue rq = Volley.newRequestQueue(getContext());
            rq.add(stringRequest);

        String nAway= namaAway.getText().toString().replaceAll("\\s+","_");
        String url1= "https://www.thesportsdb.com/api/v1/json/1/searchplayers.php?t="+nAway;
        System.out.println(url);
        StringRequest stringRequest1 = new StringRequest(Request.Method.GET, url1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("player");
                    for (int i=0; i< jsonArray.length(); i++){
                        object=jsonArray.getJSONObject(i);
                        String image = object.getString("strCutout");
                        String nama = object.getString("strPlayer");
                        String stat = object.getString("strPosition");
                        String team = object.getString("strTeam");
                        String desc = object.getString("strDescriptionEN");
                        String weight= object.getString("strTeam");
                        String height = object.getString("strTeam");
                        String born = object.getString("dateBorn");
                        String loc = object.getString("strBirthLocation");
                        String nation = object.getString("strNationality");
                        ap.add(new matchplayer(image,nama,stat,team,desc,weight,height,born,loc,nation));
                        System.out.println(ap.get(i).getNamap());
                    }
                    rvaa= new AdapterRVAplayer(ap);
                    rvaa.notifyDataSetChanged();
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                    rvAP.setLayoutManager(layoutManager);
                    rvAP.setAdapter(rvaa);
                    rvaa.setOnItemClickCallback(new AdapterRVAplayer.OnItemClickCallback() {
                        @Override
                        public void onClick(matchplayer mp) {
                            try {
                                ((MainActivity)getActivity()).loadPlayerProfile(mp.getNamap());
                                Toast.makeText(getContext(), mp.getNamap(), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.getMessage();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue rq1 = Volley.newRequestQueue(getContext());
        rq1.add(stringRequest1);
    }



    public void loadMatchData(){
        if (live==0) {
            String url = "https://www.thesportsdb.com/api/v1/json/1/lookupevent.php?id=" + ((MainActivity) getActivity()).currSkor.getId();
            System.out.println(url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        JSONArray jsonArray = object.getJSONArray("events");
                        object = jsonArray.getJSONObject(0);
                        Log.e("error", object.toString());
                        try {
                            try {
                                shotsHome.setText(object.getInt("intHomeShots") + "");
                            } catch (Exception e) {
                                shotsHome.setText("-");
                            }
                            try {
                                shotsAway.setText(object.getInt("intAwayShots") + "");
                            } catch (Exception e) {
                                shotsAway.setText("-");
                            }

                            String temp = object.getString("strHomeYellowCards");
                            String[] listYellowHome = temp.split(";");
                            yellowHome.setText(listYellowHome.length + "");

                            temp = object.getString("strAwayYellowCards");
                            String[] listYellowAway = temp.split(";");
                            yellowAway.setText(listYellowAway.length - 1 + "");

                            temp = object.getString("strHomeRedCards");
                            String[] listRedHome = temp.split(";");
                            redHome.setText(listRedHome.length - 1 + "");

                            temp = object.getString("strAwayRedCards");
                            String[] listRedAway = temp.split(";");
                            redAway.setText(listRedAway.length - 1 + "");

                            temp = object.getString("strHomeGoalDetails");
                            listGoalHome.setText(temp.replace(";", "\n"));

                            temp = object.getString("strAwayGoalDetails");
                            listGoalAway.setText(temp.replace(";", "\n"));

                            loadHomeTeamLogo(((MainActivity) getActivity()).currSkor.getIdHome());

                        } catch (Exception e) {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getContext(), "Error load upcoming match", Toast.LENGTH_SHORT).show();
                }
            });

            RequestQueue rq = Volley.newRequestQueue(getContext());
            rq.add(stringRequest);
        }
        else {
            String url = "https://www.thesportsdb.com/api/v1/json/4013017/latestsoccer.php";
            System.out.println(url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject object = new JSONObject(response);
                        JSONArray jsonArray = object.getJSONObject("teams").getJSONArray("Match");
                        object = jsonArray.getJSONObject(((MainActivity)getActivity()).pos);
                        Log.e("error", object.toString());
                        try {
                            try {
                                shotsHome.setText(object.getInt("intHomeShots") + "");
                            } catch (Exception e) {
                                shotsHome.setText("-");
                            }
                            try {
                                shotsAway.setText(object.getInt("intAwayShots") + "");
                            } catch (Exception e) {
                                shotsAway.setText("-");
                            }

                            String temp = object.getString("HomeTeamYellowCardDetails");
                            String[] listYellowHome = temp.split(";");
                            yellowHome.setText(listYellowHome.length + "");

                            temp = object.getString("AwayTeamYellowCardDetails");
                            String[] listYellowAway = temp.split(";");
                            yellowAway.setText(listYellowAway.length - 1 + "");

                            temp = object.getString("HomeTeamRedCardDetails");
                            String[] listRedHome = temp.split(";");
                            redHome.setText(listRedHome.length - 1 + "");

                            temp = object.getString("AwayTeamRedCardDetails");
                            String[] listRedAway = temp.split(";");
                            redAway.setText(listRedAway.length - 1 + "");

                            temp = object.getString("HomeGoalDetails");
                            if (temp.replace(";", "\n").equals("{}")){
                                listGoalHome.setText("");
                            }
                            else {
                                listGoalHome.setText(temp.replace(";", "\n"));
                            }

                            temp = object.getString("AwayGoalDetails");
                            if (temp.replace(";", "\n").equals("{}")){
                                listGoalAway.setText("");
                            }
                            else {
                                listGoalAway.setText(temp.replace(";", "\n"));
                            }

                            loadHomeTeamLogo(((MainActivity) getActivity()).currSkor.getIdHome());

                        } catch (Exception e) {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getContext(), "Error load upcoming match", Toast.LENGTH_SHORT).show();
                }
            });

            RequestQueue rq = Volley.newRequestQueue(getContext());
            rq.add(stringRequest);
        }
    }

    public void loadHomeTeamLogo(int home){
        String url =  "https://www.thesportsdb.com/api/v1/json/1/lookupteam.php?id="+home;
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("teams");
                    object=jsonArray.getJSONObject(0);
                    String url = object.getString("strTeamBadge");
                    new DownloadImage(logoHome)
                            .execute(url);

                    loadAwayTeamLogo(((MainActivity) getActivity()).currSkor.getIdAway());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(stringRequest);
    }
    public void loadAwayTeamLogo(int away){
        String url =  "https://www.thesportsdb.com/api/v1/json/1/lookupteam.php?id="+away;
        System.out.println(url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    JSONArray jsonArray = object.getJSONArray("teams");
                    object=jsonArray.getJSONObject(0);
                    String url = object.getString("strTeamBadge");
                    new DownloadImage(logoAway)
                            .execute(url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(stringRequest);
    }

    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}
