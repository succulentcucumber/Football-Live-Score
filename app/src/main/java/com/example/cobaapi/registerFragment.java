package com.example.cobaapi;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class registerFragment extends Fragment {


    databaseUserDAO databaseUserDAO;
    userDAO userDAO;

    EditText edUsername, edNama, edPassword;
    Button btnRegister,btnLogin;
    public registerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.register_fragment, container, false);

        edNama = (EditText) v.findViewById(R.id.edNamaRegister);
        edUsername = (EditText) v.findViewById(R.id.edUsernameRegister);
        edPassword = (EditText) v.findViewById(R.id.edPasswordRegister);
        btnRegister = (Button) v.findViewById(R.id.btnRegisterRegister);
        btnLogin=(Button)v.findViewById(R.id.btnRegisterLogin);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edUsername.getText().toString();
                String nama = edNama.getText().toString();
                String password = edPassword.getText().toString();

                if (!username.equals("")){
                    if (!nama.equals("")){
                        if (!password.equals("")){
                            ArrayList<user> listUser = ((loginRegis) getActivity()).listUser;
                            boolean ada = false;
                            for (int i=0;i<listUser.size();i++){
                                if (listUser.get(i).username.equals(username)){
                                    ada = true;
                                    break;
                                }
                            }
                            if (ada == false){
                                user baru = new user(username, nama, password);
                                ((loginRegis) getActivity()).insertUser(baru);
                                edNama.setText("");
                                edUsername.setText("");
                                edPassword.setText("");
                            } else {
                                Toast.makeText(getContext(), "Username sudah terpakai sebelumnya", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), "Field Password Belum terisi", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Field Nama Belum terisi", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Field Username Belum terisi", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((loginRegis) getActivity()).openFragment(new loginFragment());
            }
        });

        return v;
    }

}
