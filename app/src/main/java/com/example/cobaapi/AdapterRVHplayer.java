package com.example.cobaapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.ArrayList;

public class AdapterRVHplayer extends RecyclerView.Adapter<AdapterRVHplayer.RVHplayerViewHolder> {

    ArrayList<matchplayer> hplayer= new ArrayList<>();

    public AdapterRVHplayer(ArrayList<matchplayer> hplayer) {
        this.hplayer = hplayer;
    }

    private AdapterRVHplayer.OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(AdapterRVHplayer.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    @NonNull
    @Override
    public RVHplayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View view= inflater.inflate(R.layout.playerinmatch,parent,false);
        return new AdapterRVHplayer.RVHplayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RVHplayerViewHolder holder, final int position) {
        new AdapterRVHplayer.DownloadImageTask((ImageView) holder.imPlayer)
                .execute(hplayer.get(position).getImgp());
        holder.nPlayer.setText(hplayer.get(position).getNamap());
        holder.sPlayer.setText(hplayer.get(position).getStatp());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onClick(
                        hplayer.get(holder.getAdapterPosition())
                );
            }
        });
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public int getItemCount() {
        return hplayer.size();
    }

    public class RVHplayerViewHolder extends RecyclerView.ViewHolder {
        ImageView imPlayer;
        TextView nPlayer,sPlayer;
        public RVHplayerViewHolder(@NonNull View itemView) {
            super(itemView);
            imPlayer= itemView.findViewById(R.id.imPlayer);
            nPlayer= itemView.findViewById(R.id.tvHplayer);
            sPlayer= itemView.findViewById(R.id.statHplayer);
        }
    }

    public interface OnItemClickCallback{
        void onClick(matchplayer mp);
    }
}
